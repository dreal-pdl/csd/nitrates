# A propos

Ce projet contient le code de l'application shiny de valorisation des mesures de présence de nitrates dans les cours d'eau et les nappes des Pays de la Loire.

- le répertoire **data-raw** contient les scripts R de chargement et de préparation des données ;

- le répertoire **app** contient le code de l'application Shiny.


- l'application se trouve à l'adresse suivante : http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/nitrates_eau/


