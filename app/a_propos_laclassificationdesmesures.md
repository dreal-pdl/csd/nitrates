# La classification des mesures et indicateurs utilisés    


Une mesure est retenue si elle dépasse le seuil de détection et reste inférieur au seuil de saturation. Les concentrations situées en-dessous du seuil de qualification (0,5 mg/L pour les laboratoires agréés) sont tout de même retenues et une valeur forfaitaire leur est attribuée.


Afin de traduire l’ensemble des mesures sur une année, c’est le percentile 90 (ou P90) qui est utilisé (selon méthode de l’arrêté du 5 mai 2015). La règle du percentile 90 consiste à prendre en compte la valeur en deçà de laquelle se situent 90 % des mesures réalisées au cours de l’année considérée. Lorsque dix mesures ou moins ont été réalisées au total lors de la campagne, la teneur en nitrates retenue est la valeur maximale.


Ainsi: 


* si l’on dispose de moins de 10 valeurs (strict), la valeur maximale sera retenue ;
    
    
* si l’on dispose de 10 à 20 valeurs, la 2ème valeur maximale sera retenue ;
    
    
* si l’on dispose de 20 à 30 valeurs, la 3ème valeur maximale… etc

Exemple :

<img style="float: left;margin:5px 20px 5px 0px" src="exemple_P90.png" width="100%">
    

Les __moyennes__ ne sont pas utilisées car elles peuvent être tirées par les extrêmes.
Les __médianes__ ne représentent pas non plus assez justement le risque de valeur hautes qu’il convient de prendre en compte.


Une __moyenne des P90__ est réalisée pour représenter cet indicateur à l’échelle de la masse d’eau (bassin versant).


Enfin, une __tendance__ est calculée uniquement si la station dispose d’un P90 une période de 10 ans. Elle est calculée pour 2016 (période 2007-2016), pour 2017 (période 2008-2017) et pour 2018 (période 2009-2018).


Si cette condition est remplie, on distingue alors les cas suivants : 


* __pas de tendance__ : les P90 ont oscillé de plus de 5mg/l (pour une moyenne proche des 50 mg/l) et de plus de 3 mg/l (pour une moyenne autour de 10 mg/l) autour de la moyenne sur la période ou bien le coefficient de détermination de la droite de régression linéaire  <0,4 ;


* __stabilité__ : les P90 sont restés proches de la moyenne dans la fourchette indiquée ci avant ;


* __hausse ou baisse__ : le coefficient de détermination de la droite de régression linéaire montre une faible dispersion des P90 autour de celle-ci (coefficient retenu = 0,4), la variation est au moins de l’ordre du mg/l par an.

