# Les stations de mesures


Les stations de mesures des nitrates où sont effectués les prélèvements appartiennent à différents réseaux :


* le __suivi des milieux__ au titre de la directive cadre sur l’eau (DCE), comptant aussi pour le suivi de la directive nitrate (agence de l’eau et éventuels réseaux locaux), destiné à connaître l’état général des cours d’eau et nappes souterraines ;


* le __suivi des captages d’eau__ destinés à la consommation humaine (ARS), destiné principalement au contrôle de la teneur en nitrates vis-à-vis de la production d’eau potable, dans les eaux « brutes » (non traitées).


Parmi ce dernier réseau, sont identifiés :


* les __captages « eau potables »__, soient les captages dont l’usage est soit l’alimentation en eau potable, soit destiné à l’industrie agro-alimentaire, soit des usages privés ;


* les __47 captages prioritaires__ de la région, dont 45 figurent dans le bassin Loire-Bretagne et donc dans le SDAGE éponyme.


Les donnés peuvent être représentées :


* à l’__échelle régionale__, du __SAGE__ (mais ne seront pas intégrées les stations situées hors région) ou du __bassin versant de masse d’eau__, pour les points de mesure superficiels ;


* à l’__échelle régionale__ pour les points de mesure souterrains.



<img style="margin:5px 20px 5px 0px" src="2017_SAGE_PDL.png" width="75%">
