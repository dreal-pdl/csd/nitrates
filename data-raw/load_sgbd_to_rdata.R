
# librairies -------
library(datalibaba)
library(tidyverse)
library(sf)
library(lubridate)
library(rmapshaper)
library(readxl)

rm(list=ls())


# chargement des données ------------
nitrate_analyse <- importer_data(db = "si_eau",
                               schema = "nitrates",
                               table = "nitrate_analyse_v0_13") # 73046 obs

nitrate_prelevement <- importer_data(db = "si_eau",
                                     schema = "nitrates",
                                     table = "nitrate_prelevement_v0_13") # 72693 obs

station_esu <- importer_data(db = "si_eau",
                                     schema = "stations",
                                     table = "station_esu")  # 3090 obs
station_eso <- importer_data(db = "si_eau",
                                     schema = "stations",
                                     table = "station_eso") # 1996 obs

n_sage_r52 <- importer_data(db = "si_eau",
                            schema = "zonages_de_gestion",
                            table = "n_sage_r52") # 24 obs

n_masse_eau_bassin_versant_loire_bretagne <- importer_data(db = "si_eau",
                                                           schema = "hydrographie",
                                                           table = "n_masse_eau_bassin_versant_loire_bretagne") # 2039 obs

r_zones_actions_renforcees_r52 <- importer_data(db = "si_eau",
                                                schema = "hydrographie",
                                                table = "r_zones_actions_renforcees_r52")

n_region_exp_r52 <- importer_data(db = "referentiels",
                                  schema = "adminexpress",
                                  table = "n_region_exp_r52")


# table des masses d'eau bassins versants des Pays de la loire et formatage -----
masse_eau_bassin_versant_PDL<-st_join(n_masse_eau_bassin_versant_loire_bretagne, n_region_exp_r52, left = FALSE) %>% 
  select(cd_eu_masse_eau, nom_masse_eau) %>% 
  mutate_if(is.factor,as.character) %>% 
  rename(code_bassin_versant = cd_eu_masse_eau) %>% 
  rename(nom_bassin_versant = nom_masse_eau) # 457 obs

masse_eau_bassin_versant_PDL_simplifiee<-ms_simplify(masse_eau_bassin_versant_PDL,keep = .05)


# formatage table n_sage_r52 -----------
n_sage_r52 <- n_sage_r52 %>%
  rename(nom_sage = nom, code_sage = code) %>%  
  mutate(nom_sage = as.character(nom_sage)) %>% 
  mutate(nom_sage=ifelse(nom_sage == "AUZANCE VERTONNE ET COURS D'EAU CÔTIERS",
                         "AUZANCE VERTONNE ET COURS D'EAU COTIERS",
                         nom_sage))


# correspondance entre bassin_versant et SAGE (problème avec la table station) --------
# station <- importer_data(db = "si_eau",
#                              schema = "stations",
#                              table = "station")

station <- bind_rows(station_esu, station_eso %>% select(-code_sise_eaux))

correspondance_sage_bassin_versant <- station %>%
  st_drop_geometry() %>%
  select(c(code_sage, code_bassin_versant)) %>%
  unique() %>%
  filter(code_bassin_versant != is.na(code_bassin_versant)) %>%
  left_join((n_sage_r52 %>%
               select(code_sage, nom_sage) %>%
               st_drop_geometry()), by = c("code_sage")) %>%
  left_join(masse_eau_bassin_versant_PDL %>%
              st_drop_geometry(), by = c("code_bassin_versant")) %>%
  mutate_if(is.factor,as.character) %>% 
  mutate(nom_sage = ifelse(is.na(nom_sage), "HORS SAGE", nom_sage)) %>% 
  filter(!is.na(nom_bassin_versant))


# table station : mention du département, et restriction au périmètre régional et aux géométries définies -------
station_esu_PDL <- station_esu %>% 
  mutate(code_commune = as.character(code_commune)) %>% 
  mutate(insee_dep = case_when(startsWith(code_commune,"44")~ 44,
                               startsWith(code_commune,"49")~ 49,
                               startsWith(code_commune,"53")~ 53,
                               startsWith(code_commune,"72")~ 72,
                               startsWith(code_commune,"85")~ 85)) %>% 
  filter(!is.na(insee_dep)) %>%
  mutate(insee_dep = as.factor(insee_dep)) %>%
  mutate(test = st_is_empty(the_geom)) %>% 
  filter(test == FALSE) %>% 
  select(-c(station_reference_pesticides, test)) # 1481 obs

station_eso_PDL <- station_eso %>% 
  mutate(code_commune = as.character(code_commune)) %>% 
  mutate(insee_dep = case_when(startsWith(code_commune,"44")~ 44,
                               startsWith(code_commune,"49")~ 49,
                               startsWith(code_commune,"53")~ 53,
                               startsWith(code_commune,"72")~ 72,
                               startsWith(code_commune,"85")~ 85)) %>% 
  filter(!is.na(insee_dep)) %>%
  mutate(insee_dep = as.factor(insee_dep)) %>%
  mutate(test = st_is_empty(the_geom)) %>% 
  filter(test == FALSE) %>% 
  select(-c(station_reference_pesticides, test)) # 1991 obs


# création de la table nitrates ---------
nitrates <- inner_join(nitrate_prelevement,
                       nitrate_analyse,
                       by = c("code_prelevement")) %>% 
  mutate(annee = year(date_prelevement), mois = month(date_prelevement)) # 73018 obs

nitrates_esu <- nitrates %>% 
  inner_join(station_esu_PDL %>% 
               st_drop_geometry(),
             by = c("code_station")) %>% 
  rename(code_intervenant_prelevement = "code_intervenant.x",
         code_intervenant_analyse = "code_intervenant.y",
         source_prelevement = "source.x",
         source_station = "source.y") %>% 
  select(code_station, libelle_station, source_station,
         code_prelevement, source_prelevement,
         date_prelevement, heure_prelevement, annee, mois,
         id_usage,
         code_analyse, date_analyse,
         resultat_analyse, code_remarque) #%>% # 52259 obs
# filter(!is.na(resultat_analyse)) # sans filtre

nitrates_eso <- nitrates %>% 
  inner_join(station_eso_PDL %>% 
               st_drop_geometry(),
             by = c("code_station")) %>% 
  rename(code_intervenant_prelevement = "code_intervenant.x",
         code_intervenant_analyse = "code_intervenant.y",
         source_prelevement = "source.x",
         source_station = "source.y") %>% 
  select(code_station, libelle_station, source_station,
         code_prelevement, source_prelevement,
         date_prelevement, heure_prelevement, annee, mois,
         id_usage,
         code_analyse, date_analyse,
         resultat_analyse, code_remarque) # %>% # 18451 obs
# filter(!is.na(resultat_analyse)) # sans filtre

nitrates <- bind_rows(nitrates_esu %>% mutate(code_nature_eau = "ESU"),
                      nitrates_eso %>% mutate(code_nature_eau = "ESO")) # 70710 obs

# analyse_data_nitrates_v0_13 <- nitrates %>%
#   group_by(annee, code_nature_eau, source_station, source_prelevement) %>%
#   summarise(nbre_lignes = n()) %>%
#   ungroup() %>%
#   group_by(annee, code_nature_eau) %>%
#   mutate(total = sum(nbre_lignes), pourcentage = round(nbre_lignes/total*100, digits = 1)) %>%
#   ungroup()
# rm (analyse_data_nitrates_v0_13)


# sauvegarde ---------
rm(station_esu, 
   station_eso, 
   station,
   n_masse_eau_bassin_versant_loire_bretagne,
   nitrate_analyse,
   nitrate_prelevement,
   nitrates_esu,
   nitrates_eso)

save.image(file = "data/load_sgbd_to_rdata_v0_13_sansfiltre.RData")


# creation de fichiers csv ------
date <- Sys.Date()
write.csv2(nitrate_prelevement, file = paste0("nitrate_prelevement_v0_13",date,".csv"))

