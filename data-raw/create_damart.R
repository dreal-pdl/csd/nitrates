
# librairies ----------
library(tidyverse)
library(sf)
library(datalibaba)
library(readxl)

rm(list=ls())


# chargement de load_sgbd_to_rdata.RData --------------
load("data/load_sgbd_to_rdata_v0_13_sansfiltre.RData")

# unique(nitrates$annee)
# [1] 2007 2008 2009 2010 2011 2012 2013 2014 2015 2016 2017 2018 2019 2020 2021


# restriction de la table des stations aux mesures existantes -------
station_pdl<-bind_rows(station_esu_PDL %>% 
                         mutate(code_nature_eau = "ESU"),
                       station_eso_PDL %>%
                         mutate(code_nature_eau = "ESO") %>%
                         select(-code_sise_eaux)) %>% 
  semi_join(nitrates, by = c("code_station")) # 1449 obs


# création de listes pour l'application RShiny ----
Sage <- correspondance_sage_bassin_versant %>% 
  select(code_sage,  nom_sage) %>%
  arrange(nom_sage) %>% 
  pull(nom_sage) %>% 
  unique()
Sage <- as.factor(c("Tous", Sage))

BassinVersant <- correspondance_sage_bassin_versant %>% 
  select(code_bassin_versant, nom_bassin_versant) %>% 
  arrange(nom_bassin_versant) %>% 
  pull(nom_bassin_versant) %>% 
  unique()
BassinVersant <- as.factor(c("Tous", BassinVersant))

Station <- station_pdl %>%
  select(libelle_station) %>% 
  mutate(libelle_station = as.character(libelle_station)) %>% 
  arrange(libelle_station) %>% 
  pull(libelle_station)
Station <- as.factor(c("Toutes", Station))

NatureEau <- station_pdl %>% 
  select(code_nature_eau) %>% 
  mutate(code_nature_eau = as.character(code_nature_eau)) %>% 
  pull(code_nature_eau) %>% 
  unique()
NatureEau <- as.factor(NatureEau)


# calcul P90 ----------------
nitrates_P90 <- nitrates %>%
  group_by(code_station, annee) %>%
  arrange(code_station, annee, desc(resultat_analyse)) %>%
  mutate(n = n(),
         x = 1,
         pos = cumsum(x),
         pos_a_garder = floor(n/10) + 1
         )%>%
  filter(pos_a_garder == pos) %>%
  rename(P90 = resultat_analyse) %>%
  select(-c(n, x, pos)) %>%
  ungroup %>%
  mutate(classe = case_when(P90<10 ~ "0-10 mg/l",
                            P90<18 ~ "10-18 mg/l",
                            P90<30 ~ "18-30 mg/l",
                            P90<40 ~ "30-40 mg/l",
                            P90<50 ~ "40-50 mg/l",
                            P90<60 ~ "50-60 mg/l",
                            P90<70 ~ "60-70 mg/l",
                            P90>=70 ~ "70 mg/l et plus")) %>%
  select(-c(code_prelevement, source_prelevement, date_prelevement, heure_prelevement, mois,
            code_analyse, date_analyse, code_remarque)) %>%
  filter(!is.na(classe)) %>% 
  mutate_if(is.character,as.factor) # 10840 obs dont 5008 esu et 5832 eso


# calcul de la tendance P90 -----------------------------
creer_tendanceP90 <- function(annee){
  annee_max <- annee
  annee_min <- annee_max - 9
  
  # qualification des 5 années contigües minimum
  nitrates_tendanceP90 <- nitrates_P90 %>%
    select(-c(id_usage, pos_a_garder)) %>% 
    filter(annee_min <= annee & annee <= annee_max) %>%
    group_by(code_station) %>%
    arrange(code_station, annee) %>% 
    mutate(contingent = case_when(
      is.na(lag(annee)) ~ 0,
      annee-lag(annee) == 1 ~ 0,
      TRUE ~ 1),
      contingent_id = cumsum(contingent) + 1) %>%
    ungroup() %>%
    group_by(code_station, contingent_id) %>%
    add_tally() %>% 
    mutate(calcul_tendance = case_when(n<5 ~ "données insuffisantes",
                                       T ~ "données suffisantes")) %>% 
    ungroup() %>% 
    select(-c(contingent, contingent_id, n)) 
  
  # calcul du coefficient de détermination (R²) et de la pente de la droite a (y = a x + b)
  stat <- nitrates_tendanceP90 %>%
    filter(calcul_tendance == "données suffisantes") %>%
    select(code_station, annee, P90) %>%
    group_by(code_station) %>% 
    do(Mods = lm(P90 ~ annee, data = .)) %>% # do() permet de calculer à l'intérieur de group_by()
    mutate(determination = as.numeric(summary(Mods)[8])) %>% 
    mutate(pente = coef(Mods)[2]) %>%
    ungroup() %>% 
    select(-c(Mods)) %>% 
    mutate(calcul_tendance = "données suffisantes")
  
  # calcul de la tendance pour P90 par station
  stat1 <- nitrates_tendanceP90 %>% 
    left_join(stat,by = c("code_station", "calcul_tendance")) %>% 
    group_by(code_station, calcul_tendance) %>% 
    mutate(v = pente/first(P90)) %>% 
    mutate(mean = mean(P90)) %>% 
    mutate(moyenne = case_when(calcul_tendance == "données insuffisantes" ~ -1,
                             T ~ mean)) %>% 
    ungroup()
  
  stat2 <- stat1 %>% 
    filter(calcul_tendance == "données insuffisantes") %>% 
    rename(tendance = calcul_tendance) %>% 
    select(-c(annee, P90, classe, mean)) %>% 
    unique()
  
  stat3 <- stat1 %>% 
    filter(calcul_tendance != "données insuffisantes") %>% 
    group_by(code_station) %>% 
    mutate(tendance = case_when(abs(pente)<=1 & max((max(P90) - moyenne),(moyenne - min(P90)))>(-0.005*moyenne+0.35)*moyenne ~ "pas de tendance",
                                abs(pente)<=1 & max((max(P90) - moyenne),(moyenne - min(P90)))<=(-0.005*moyenne+0.35)*moyenne ~ "stable",
                                abs(pente)>1 & determination<0.4 ~ "pas de tendance",
                                abs(pente)>1 & determination>=0.4 & v<=-0.05 ~ "baisse",
                                abs(pente)>1 & determination>=0.4 & v<0 ~ "baisse",
                                abs(pente)>1 & determination>=0.4 & v<0.05 ~ "hausse",
                                abs(pente)>1 & determination>=0.4 & v>=0.05 ~ "hausse")) %>% 
    select(-c(annee, P90, classe, calcul_tendance, mean)) %>%
    unique() %>%
    ungroup()
  
  
  stat4 <- anti_join(stat2, stat3, by = "code_station")  
  
  nitrates_tendanceP90 <- bind_rows(stat3, stat4) %>% 
    mutate(periode = paste(annee_min,"-",annee_max)) %>% 
    mutate_if(is.character,as.factor)
  
  return(nitrates_tendanceP90)
  
}

# nitrates_tendanceP90_2017<-creer_tendanceP90(2017)

# nitrates_tendanceP90<-map(c(2016:2018), ~creer_tendanceP90(.x)) %>% 
# nitrates_tendanceP90<-map(c(2016:2019), ~creer_tendanceP90(.x)) %>%
# nitrates_tendanceP90<-map(c(2016:2020), ~creer_tendanceP90(.x)) %>%
nitrates_tendanceP90<-map(c(2016:2021), ~creer_tendanceP90(.x)) %>%
  bind_rows() # 7470 obs dont 3794 esu et 3676 eso

nitrates_tendanceP90 <- nitrates_tendanceP90 %>% 
  select(code_station, libelle_station, code_nature_eau, tendance, periode) %>%
  mutate(tendance = as.character(tendance))
f_tendanceP90 <- fct_relevel(c("données insuffisantes", "pas de tendance", "baisse", "stable", "hausse"))
nitrates_tendanceP90$tendance <- factor(nitrates_tendanceP90$tendance, levels = f_tendanceP90)

# periode_nitrates_tendanceP90<-data.frame(annee = c(2007:2018)) %>%
# periode_nitrates_tendanceP90<-data.frame(annee = c(2007:2019)) %>%
# periode_nitrates_tendanceP90 <- data.frame(annee = c(2007:2020)) %>%
periode_nitrates_tendanceP90 <- data.frame(annee = c(2007:2021)) %>%
  mutate(periode = case_when(annee<= 2016 ~ "2007 - 2016",
                             annee > 2016 ~ paste(annee - 9,"-", annee))
         ) %>% 
  mutate(annee = as.factor(annee), periode = as.factor(periode))


# calcul de la moyenne et du maximum des P90 sur un bassin versant ----------
nitrates_bassin_versant <- nitrates_P90 %>%
  semi_join(station_pdl) %>% 
  left_join(station_pdl %>% 
              select(c(code_station, libelle_station, code_bassin_versant)) %>%
              st_drop_geometry(),
            by = c("code_station")) %>% 
  group_by(code_bassin_versant, annee, code_nature_eau) %>% 
  summarise(moyP90 = round(mean(P90), digits = 2), maxP90 = round(max(P90), digits = 2)) %>% 
  ungroup()

nitrates_moyP90_bassin_versant <- nitrates_bassin_versant %>% 
  select(-c(maxP90)) %>%
  mutate(classe = case_when(moyP90<10 ~ "0-10 mg/l",
                            moyP90<18 ~ "10-18 mg/l",
                            moyP90<30 ~ "18-30 mg/l",
                            moyP90<40 ~ "30-40 mg/l",
                            moyP90<50 ~ "40-50 mg/l",
                            moyP90<60 ~ "50-60 mg/l",
                            moyP90<70 ~ "60-70 mg/l",
                            moyP90>=70 ~ "70 mg/l et plus"))

nitrates_maxP90_bassin_versant <- nitrates_bassin_versant %>% 
  select(-c(moyP90)) %>%
  mutate(classe = case_when(maxP90<10 ~ "0-10 mg/l",
                            maxP90<18 ~ "10-18 mg/l",
                            maxP90<30 ~ "18-30 mg/l",
                            maxP90<40 ~ "30-40 mg/l",
                            maxP90<50 ~ "40-50 mg/l",
                            maxP90<60 ~ "50-60 mg/l",
                            maxP90<70 ~ "60-70 mg/l",
                            maxP90>=70 ~ "70 mg/l et plus"))

rm(nitrates_bassin_versant)


# répartition P90 ------------
repartition_P90_region <- nitrates_P90 %>%
  semi_join(station_pdl) %>% 
  left_join(station_pdl %>% 
              select(code_station, code_sage, code_bassin_versant, insee_dep) %>%
              st_drop_geometry()) %>% 
  group_by(code_nature_eau, annee, classe) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits=1), sum = sum(n)) %>% 
  ungroup() %>% 
  mutate(nom_sage = "Tous", nom_bassin_versant = "Tous")

repartition_P90_sage <- nitrates_P90 %>%
  semi_join(station_pdl) %>% 
  left_join(station_pdl %>% 
              select(code_station, code_sage, code_bassin_versant, insee_dep) %>%
              st_drop_geometry()) %>% 
  group_by(code_nature_eau, code_sage, annee, classe) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits=1), sum = sum(n)) %>% 
  ungroup() %>% 
  left_join(n_sage_r52 %>% 
              select(code_sage, nom_sage) %>% 
              st_drop_geometry()) %>% 
  mutate(nom_bassin_versant = "Tous") %>% 
  mutate(nom_sage = as.character(nom_sage)) %>% 
  mutate(nom_sage = ifelse(is.na(nom_sage), "HORS SAGE", nom_sage))

repartition_P90_bassin_versant <- nitrates_P90 %>%
  semi_join(station_pdl) %>% 
  left_join(station_pdl %>% 
              select(code_station, code_sage, code_bassin_versant, insee_dep) %>%
              st_drop_geometry()) %>% 
  group_by(code_nature_eau, code_bassin_versant, annee, classe) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits=1), sum = sum(n)) %>% 
  ungroup() %>% 
  left_join(masse_eau_bassin_versant_PDL %>% 
              st_drop_geometry()) %>% 
  left_join(correspondance_sage_bassin_versant) %>% 
  left_join(n_sage_r52 %>% 
              select(code_sage, nom_sage) %>% 
              st_drop_geometry()) 

repartition_P90 <- bind_rows(repartition_P90_region,
                             repartition_P90_sage,
                             repartition_P90_bassin_versant) %>% 
  select(-c(code_sage, code_bassin_versant)) %>% 
  rename(nbre_station_par_classe = n, nbre_station_total = sum)

rm(repartition_P90_region,
   repartition_P90_sage,
   repartition_P90_bassin_versant)


# répartition tendance P90 -----------
repartition_tendanceP90_region <- nitrates_tendanceP90 %>% 
  semi_join(station_pdl) %>% 
  group_by(code_nature_eau, periode, tendance) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits = 1), sum = sum(n)) %>% 
  ungroup() %>% 
  rename(nbre_station_par_tendance = n, nbre_station_total = sum) %>% 
  mutate(nom_sage = "Tous", nom_bassin_versant = "Tous")

repartition_tendanceP90_sage <- nitrates_tendanceP90 %>% 
  semi_join(station_pdl) %>% 
  left_join(station_pdl %>% 
              select(code_station, code_sage, code_bassin_versant) %>%
              st_drop_geometry()) %>% 
  group_by(code_nature_eau, code_sage, periode,tendance) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits = 1), sum = sum(n)) %>% 
  ungroup() %>% 
  rename(nbre_station_par_tendance = n, nbre_station_total = sum) %>% 
  left_join(n_sage_r52 %>% 
              select(code_sage,nom_sage) %>% 
              st_drop_geometry()) %>% 
  mutate(nom_bassin_versant = "Tous") %>% 
  select(-c(code_sage)) %>%
  mutate(nom_sage = as.character(nom_sage)) %>% 
  mutate(nom_sage = ifelse(is.na(nom_sage), "HORS SAGE", nom_sage))

repartition_tendanceP90_bassin_versant <- nitrates_tendanceP90 %>% 
  semi_join(station_pdl) %>% 
  left_join(station_pdl %>% 
              select(code_station, code_sage, code_bassin_versant) %>%
              st_drop_geometry()) %>% 
  group_by(code_nature_eau, code_bassin_versant, periode, tendance) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits = 1), sum = sum(n)) %>%  
  ungroup() %>% 
  rename(nbre_station_par_tendance = n, nbre_station_total = sum) %>% 
  left_join(masse_eau_bassin_versant_PDL %>% 
              st_drop_geometry()) %>%
  left_join(correspondance_sage_bassin_versant) %>% 
  left_join(n_sage_r52 %>% 
              select(code_sage, nom_sage) %>% 
              st_drop_geometry()) %>% 
  select(-c(code_sage, code_bassin_versant))

repartition_tendanceP90<-bind_rows(repartition_tendanceP90_region,
                                   repartition_tendanceP90_sage,
                                   repartition_tendanceP90_bassin_versant)

rm(repartition_tendanceP90_region,
   repartition_tendanceP90_sage,
   repartition_tendanceP90_bassin_versant)


# répartition moyenne BV --------
repartition_moyBV_region <- nitrates_moyP90_bassin_versant %>% 
  group_by(code_nature_eau, annee, classe) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits = 1), sum = sum(n)) %>% 
  ungroup() %>% 
  rename(nbre_bv_par_classe = n, nbre_bv_total = sum) %>% 
  mutate(nom_sage = "Tous")

repartition_moyBV_sage <- nitrates_moyP90_bassin_versant %>%
  left_join(correspondance_sage_bassin_versant) %>% 
  filter(!is.na(code_sage)) %>%
  group_by(code_nature_eau, code_sage, annee, classe) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits = 1), sum = sum(n)) %>% 
  ungroup() %>% 
  rename(nbre_bv_par_classe = n, nbre_bv_total = sum) %>% 
  left_join(n_sage_r52 %>% select(nom_sage, code_sage) %>% st_drop_geometry()) %>% 
  select(-c(code_sage)) %>% 
  mutate(nom_sage = as.character(nom_sage)) %>% 
  mutate(nom_sage = ifelse(is.na(nom_sage), "HORS SAGE", nom_sage))

repartition_moyBV <- bind_rows(repartition_moyBV_region, repartition_moyBV_sage)

rm(repartition_moyBV_region,
   repartition_moyBV_sage)


# répartition max BV  --------
repartition_maxBV_region <- nitrates_maxP90_bassin_versant %>% 
  group_by(code_nature_eau, annee, classe) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits = 1), sum = sum(n)) %>% 
  ungroup() %>% 
  rename(nbre_bv_par_classe = n, nbre_bv_total = sum) %>% 
  mutate(nom_sage = "Tous")

repartition_maxBV_sage <- nitrates_maxP90_bassin_versant %>%
  left_join(correspondance_sage_bassin_versant) %>% 
  filter(!is.na(code_sage)) %>%
  group_by(code_nature_eau, code_sage, annee, classe) %>% 
  summarise(n = n()) %>% 
  mutate(pourc = round((n*100/sum(n)), digits = 1), sum = sum(n)) %>% 
  ungroup() %>% 
  rename(nbre_bv_par_classe = n, nbre_bv_total=sum) %>% 
  left_join(n_sage_r52 %>% select(nom_sage, code_sage) %>% st_drop_geometry()) %>% 
  select(-c(code_sage)) %>% 
  mutate(nom_sage = as.character(nom_sage)) %>% 
  mutate(nom_sage = ifelse(is.na(nom_sage), "HORS SAGE", nom_sage))

repartition_maxBV<-bind_rows(repartition_maxBV_region, repartition_maxBV_sage)

rm(repartition_maxBV_region,
   repartition_maxBV_sage)


# concentration ESU -----------------
nitrates_esu <- nitrates %>%
  semi_join(station_pdl) %>% 
  filter(code_nature_eau == "ESU") %>% 
  semi_join(station_pdl)

depassement_seuil_esu_region <- nitrates_esu %>%
  group_by(annee) %>% 
  unique() %>% 
  mutate(nbre_mesure = n()) %>% 
  ungroup() %>% 
  filter(resultat_analyse >= 18) %>% 
  group_by(annee) %>% 
  mutate(nbre_depassement_eu = n()) %>% 
  mutate(depassement_eu = round(nbre_depassement_eu*100/nbre_mesure, digits = 1),
         non_depassement_eu = 100 - depassement_eu) %>% 
  ungroup() %>% 
  select(annee, depassement_eu, non_depassement_eu) %>% 
  unique() %>% 
  mutate(nom_sage = "Tous", nom_bassin_versant = "Tous")

depassement_seuil_esu_sage <- nitrates_esu %>%
  left_join(station_pdl %>%
              select(code_station, code_sage) %>% 
              st_drop_geometry(),
            by = "code_station"
            ) %>% 
  group_by(code_sage, annee) %>% 
  unique() %>% 
  mutate(nbre_mesure = n()) %>% 
  ungroup() %>% 
  filter(resultat_analyse >= 18) %>% 
  group_by(code_sage,annee) %>% 
  mutate(nbre_depassement_eu = n()) %>% 
  mutate(depassement_eu = round(nbre_depassement_eu*100/nbre_mesure, digits = 1),
         non_depassement_eu = 100 - depassement_eu) %>% 
  ungroup() %>% 
  select(code_sage, annee, depassement_eu, non_depassement_eu) %>% 
  unique() %>% 
  left_join(n_sage_r52 %>%
              select(code_sage,nom_sage) %>% 
              st_drop_geometry()
            ) %>% 
  mutate(nom_bassin_versant = "Tous") %>% 
  mutate(nom_sage = as.character(nom_sage)) %>%
  mutate(nom_sage = ifelse(is.na(nom_sage), "HORS SAGE", nom_sage)) %>% 
  select(-c(code_sage))
  
depassement_seuil_esu_bassin_versant <- nitrates_esu %>%
  left_join(station_pdl %>%
              select(code_station, code_sage, code_bassin_versant) %>% 
              st_drop_geometry(),
            by = "code_station"
            ) %>%
  group_by(code_bassin_versant, code_sage, annee) %>% 
  unique() %>% 
  mutate(nbre_mesure = n()) %>% 
  ungroup() %>%
  filter(resultat_analyse >= 18) %>% 
  group_by(code_bassin_versant, code_sage, annee) %>% 
  mutate(nbre_depassement_eu = n()) %>% 
  mutate(depassement_eu = round(nbre_depassement_eu*100/nbre_mesure, digits = 1),
         non_depassement_eu = 100 - depassement_eu) %>% 
  ungroup() %>% 
  select(code_bassin_versant, code_sage, annee, depassement_eu, non_depassement_eu) %>% 
  unique() %>% 
  left_join(masse_eau_bassin_versant_PDL %>% 
              st_drop_geometry()) %>% 
  left_join(n_sage_r52 %>% 
              select(code_sage, nom_sage) %>% 
              st_drop_geometry()) %>% 
  mutate(nom_bassin_versant = as.character(nom_bassin_versant)) %>% 
  mutate(nom_sage = as.character(nom_sage)) %>%
  mutate(nom_sage = ifelse(is.na(nom_sage), "HORS SAGE", nom_sage)) %>% 
  select(-c(code_bassin_versant, code_sage))

depassement_seuil_esu<-bind_rows(depassement_seuil_esu_region,
                                 depassement_seuil_esu_sage,
                                 depassement_seuil_esu_bassin_versant)

depassement_seuil_esu<-depassement_seuil_esu %>% 
  mutate_if(is.character,as.factor) %>% 
  arrange(annee) %>% 
  mutate(annee = as.factor(annee))

 rm(depassement_seuil_esu_region,
    depassement_seuil_esu_sage,
    depassement_seuil_esu_bassin_versant) 
 
 
# concentration ESO -----------------
nitrates_eso <- nitrates %>%
   semi_join(station_pdl) %>% 
   filter(code_nature_eau == "ESO") %>% 
   semi_join(station_pdl)
 
 depassement_seuil_eso_region <- nitrates_eso %>%
   group_by(annee) %>% 
   unique() %>% 
   mutate(nbre_mesure = n()) %>% 
   ungroup() %>% 
   filter(resultat_analyse >= 50) %>% 
   group_by(annee) %>% 
   mutate(nbre_depassement_eo = n()) %>% 
   mutate(depassement_eo = round(nbre_depassement_eo*100/nbre_mesure, digits = 1),
          non_depassement_eo = 100 - depassement_eo) %>% 
   ungroup() %>% 
   select(annee, depassement_eo, non_depassement_eo) %>% 
   unique() %>% 
   mutate(nom_sage = "Tous", nom_bassin_versant = "Tous")
 
 depassement_seuil_eso_sage <- nitrates_eso %>%
   left_join(station_pdl %>%
               select(code_station, code_sage) %>% 
               st_drop_geometry(),
             by = "code_station"
             ) %>% 
   group_by(code_sage, annee) %>% 
   unique() %>% 
   mutate(nbre_mesure = n()) %>% 
   ungroup() %>% 
   filter(resultat_analyse >= 50) %>% 
   group_by(code_sage,annee) %>% 
   mutate(nbre_depassement_eo = n()) %>% 
   mutate(depassement_eo = round(nbre_depassement_eo*100/nbre_mesure, digits = 1),
          non_depassement_eo = 100 - depassement_eo) %>% 
   ungroup() %>% 
   select(code_sage, annee, depassement_eo, non_depassement_eo) %>% 
   unique() %>% 
   left_join(n_sage_r52 %>%
               select(code_sage, nom_sage) %>% 
               st_drop_geometry()
             ) %>% 
   mutate(nom_bassin_versant = "Tous") %>% 
   mutate(nom_sage = as.character(nom_sage)) %>%
   mutate(nom_sage = ifelse(is.na(nom_sage), "HORS SAGE", nom_sage)) %>% 
   select(-c(code_sage))
 
 depassement_seuil_eso_bassin_versant <- nitrates_eso %>%
   left_join(station_pdl %>%
               select(code_station, code_sage, code_bassin_versant) %>% 
               st_drop_geometry(),
             by = "code_station"
             ) %>%
   group_by(code_bassin_versant, code_sage, annee) %>% 
   unique() %>% 
   mutate(nbre_mesure = n()) %>% 
   ungroup() %>%
   filter(resultat_analyse >= 50) %>% 
   group_by(code_bassin_versant,code_sage,annee) %>% 
   mutate(nbre_depassement_eo = n()) %>% 
   mutate(depassement_eo = round(nbre_depassement_eo*100/nbre_mesure,digits = 1),
          non_depassement_eo = 100 - depassement_eo) %>% 
   ungroup() %>% 
   select(code_bassin_versant, code_sage, annee, depassement_eo, non_depassement_eo) %>% 
   unique() %>% 
   left_join(masse_eau_bassin_versant_PDL %>% 
               st_drop_geometry()) %>% 
   left_join(n_sage_r52 %>% 
               select(code_sage, nom_sage) %>% 
               st_drop_geometry()) %>% 
   mutate(nom_bassin_versant = as.character(nom_bassin_versant)) %>% 
   mutate(nom_sage = as.character(nom_sage)) %>%
   mutate(nom_sage = ifelse(is.na(nom_sage), "HORS SAGE", nom_sage)) %>% 
   select(-c(code_bassin_versant, code_sage))
 
 depassement_seuil_eso<-bind_rows(depassement_seuil_eso_region,
                                  depassement_seuil_eso_sage,
                                  depassement_seuil_eso_bassin_versant)
 
 depassement_seuil_eso <- depassement_seuil_eso %>% 
   mutate_if(is.character,as.factor) %>% 
   arrange(annee) %>% 
   mutate(annee = as.factor(annee))
 
 rm(depassement_seuil_eso_region,
    depassement_seuil_eso_sage,
    depassement_seuil_eso_bassin_versant) 
 
 
# # concentration AEP (PROBLEME) -----------
# 
#  depassement_seuil50_captage_aep<-captage_aep %>% 
#    left_join(nitrates_P90 %>% select(code_station,annee,P90),by=c("code_station")) %>% 
#    filter(!is.na(P90)) %>% 
#    mutate(seuil50 = ifelse(P90>=50,"supérieur ou égal à 50 mg/l","inférieur à 50 mg/l")) %>% 
#    mutate_if(is.character,as.factor) %>% 
#    # select(code_station,libelle_station,code_nature_eau,captage_prioritaire_nitrate,annee,P90,seuil50) 
#    select(code_station,libelle_station,code_nature_eau,captage_prioritaire,annee,P90,seuil50)
#  
#  repartition_seuil50_captage_aep<-depassement_seuil50_captage_aep %>%
#    st_drop_geometry() %>% 
#    group_by(annee,seuil50) %>% 
#    tally()%>% 
#    ungroup() %>%
#    complete(annee,seuil50) %>% 
#    mutate(n = ifelse(is.na(n),0,n))
#  
#  f_seuil50<-fct_relevel(c("supérieur ou égal à 50 mg/l","inférieur à 50 mg/l"))
#  repartition_seuil50_captage_aep$seuil50<-factor(repartition_seuil50_captage_aep$seuil50,levels = f_seuil50)


# choix des couleurs -----------
couleur <- c("#33CCFF", "#00CC33", "#66FF99", "#FFFF00", "orange", "red", "#FF6633", "#CC3300")
facteur <- nitrates_P90 %>% 
  arrange(classe) %>% 
  pull(classe) %>% 
  unique() %>% 
  as.character()
choix_couleur_station <- bind_cols("classe" = facteur, "couleur" = couleur)
rm(couleur, facteur)

couleur<-c("black", "#ffffcc", "#66ff33", "#66ffff", "orange")
# données insuffisantes, pas de tendance, baisse, stable, hausse
facteur <- nitrates_tendanceP90 %>% 
  arrange(tendance) %>%
  pull(tendance) %>% 
  unique() %>% 
  as.character()
choix_couleur_tendance <- bind_cols("tendance" = facteur, "couleur" = couleur)
rm(couleur, facteur)

couleur <- c("#33CCFF", "#00CC33", "#66FF99", "#FFFF00", "orange", "red", "#FF6633", "#CC3300")
facteur <- nitrates_moyP90_bassin_versant %>% 
  arrange(classe) %>% 
  pull(classe) %>% 
  unique() %>% 
  as.character()
choix_couleur_bassin_versant <- bind_cols("classe" = facteur, "couleur" = couleur)
rm(couleur, facteur)

couleur_seuil50 <- c("red","blue")


# données à télécharger ----------
nitrates_telecharger <- nitrates %>%
  semi_join(station_pdl, by = c("code_station")) %>% 
  select(annee, mois, code_nature_eau, code_station, libelle_station, resultat_analyse, code_remarque, source_prelevement) %>% 
  left_join(station_pdl %>%
              st_drop_geometry() %>%
              select(code_station,
                     code_sage,
                     code_bassin_versant,
                     insee_dep,
                     code_commune,
                     captage_prioritaire_nitrates)) %>%
  left_join(correspondance_sage_bassin_versant) %>% 
  select(annee, mois,
         code_nature_eau,
         code_sage, nom_sage,
         code_bassin_versant, nom_bassin_versant,
         code_station, libelle_station,
         source_prelevement, resultat_analyse, code_remarque,
         insee_dep, code_commune,
         captage_prioritaire_nitrates
         # captage_prioritaire
         ) %>% 
  arrange(annee, mois, code_nature_eau, nom_sage, nom_bassin_versant)

nitrates_P90_telecharger <- nitrates_P90 %>% 
  semi_join(station_pdl, by = c("code_station")) %>% 
  select(annee, code_nature_eau, code_station, libelle_station, P90, classe) %>% 
  left_join(station_pdl %>%
              st_drop_geometry() %>%
              select(code_station,
                     code_sage,
                     code_bassin_versant,
                     insee_dep,
                     code_commune,
                     captage_prioritaire_nitrates)) %>%
  left_join(correspondance_sage_bassin_versant) %>% 
  select(annee,
         code_nature_eau,
         code_sage, nom_sage,
         code_bassin_versant, nom_bassin_versant,
         code_station, libelle_station,
         P90, classe,
         insee_dep, code_commune,
         captage_prioritaire_nitrates
         # captage_prioritaire
         ) %>% 
  arrange(annee, code_nature_eau, nom_sage, nom_bassin_versant)
  
nitrates_tendanceP90_telecharger <- nitrates_tendanceP90 %>% 
  semi_join(station_pdl, by = c("code_station")) %>% 
  left_join(station_pdl %>%
              st_drop_geometry() %>%
              select(code_station,
                     code_sage,
                     code_bassin_versant,
                     insee_dep,
                     code_commune,
                     captage_prioritaire_nitrates)) %>%
  left_join(correspondance_sage_bassin_versant) %>% 
  select(periode,
         code_nature_eau,
         code_sage, nom_sage,
         code_bassin_versant, nom_bassin_versant,
         code_station, libelle_station,
         tendance,
         insee_dep, code_commune,
         captage_prioritaire_nitrates
         # captage_prioritaire
         ) %>% 
  arrange(periode, code_nature_eau, nom_sage, nom_bassin_versant)

nitrates_moyP90_bassin_versant_telecharger <- nitrates_moyP90_bassin_versant %>% 
  left_join(correspondance_sage_bassin_versant) %>% 
  select(annee,
         code_nature_eau,
         code_sage, nom_sage,
         code_bassin_versant, nom_bassin_versant,
         moyP90,
         classe) %>% 
  arrange(annee, code_nature_eau, code_sage, nom_sage, code_bassin_versant, nom_bassin_versant)

nitrates_maxP90_bassin_versant_telecharger <- nitrates_maxP90_bassin_versant %>% 
  left_join(correspondance_sage_bassin_versant) %>% 
  select(annee,
         code_nature_eau,
         code_sage, nom_sage,
         code_bassin_versant, nom_bassin_versant,
         maxP90,
         classe) %>% 
  arrange(annee, code_nature_eau, code_sage, nom_sage, code_bassin_versant, nom_bassin_versant)


# sauvegarde ----------
save.image(file = "data/create_datamart_v0_13_sansfiltre.RData")

save(nitrates_P90,
     nitrates_tendanceP90,
     nitrates_moyP90_bassin_versant,
     nitrates_maxP90_bassin_versant,
     repartition_maxBV,repartition_moyBV,
     repartition_P90,
     repartition_tendanceP90,
     depassement_seuil_esu,
     depassement_seuil_eso,
     # depassement_seuil50_captage_aep,
     # repartition_seuil50_captage_aep,
     nitrates_telecharger,
     nitrates_P90_telecharger,
     nitrates_tendanceP90_telecharger,
     nitrates_moyP90_bassin_versant_telecharger,
     nitrates_maxP90_bassin_versant_telecharger,
     file = "app/app.RData",
     version = 2
     )

save(Sage, BassinVersant, Station, NatureEau,
     correspondance_sage_bassin_versant,
     periode_nitrates_tendanceP90,
     choix_couleur_station, choix_couleur_tendance, choix_couleur_bassin_versant, couleur_seuil50,
     file = "app/referentiels.RData",
     version = 2
     )

# formatage des tables avec coordonnées géographiques
n_region_exp_r52<-st_transform(n_region_exp_r52,"+proj=longlat + datum=WGS84")
n_sage_r52<-st_transform(n_sage_r52,"+proj=longlat + datum=WGS84")
masse_eau_bassin_versant_PDL<-st_transform(masse_eau_bassin_versant_PDL,"+proj=longlat + datum=WGS84")
masse_eau_bassin_versant_PDL_simplifiee<-st_transform(masse_eau_bassin_versant_PDL_simplifiee,"+proj=longlat + datum=WGS84")
station_pdl<-st_transform(station_pdl,"+proj=longlat + datum=WGS84")
r_zones_actions_renforcees_r52<-st_transform(r_zones_actions_renforcees_r52,"+proj=longlat + datum=WGS84")

save(n_region_exp_r52,
     n_sage_r52,
     masse_eau_bassin_versant_PDL,
     masse_eau_bassin_versant_PDL_simplifiee,
     station_pdl,
     # captage_aep,
     # captage_aep_prioritaire,
     r_zones_actions_renforcees_r52,
     file = "app/referentiels_carto.RData",
     version = 2
     )


# versement sur le sgbd/datamart/nitrates ------
poster_data(data = nitrates_P90,
            db = "datamart",
            schema = "nitrates", 
            table = "nitrates_p90_v0_13",
            pk = c("code_station", "libelle_station", "annee"),
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            user = "does")
commenter_table(comment = "calcul du P90 par station",
                db = "datamart",
                schema = "nitrates",
                table = "nitrates_p90_v0_13", 
                user = "does")

poster_data(data = nitrates_tendanceP90,
            db = "datamart",
            schema = "nitrates", 
            table = "nitrates_tendancep90_v0_13",
            pk = c("code_station", "libelle_station", "periode"),
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            user = "does")
commenter_table(comment = "calcul de la tendance P90 par station",
                db = "datamart",
                schema = "nitrates",
                table = "nitrates_tendancep90_v0_13", 
                user = "does")

poster_data(data = nitrates_moyP90_bassin_versant,
            db = "datamart",
            schema = "nitrates", 
            table = "nitrates_moyp90_bassin_versant_v0_13",
            pk = c("code_bassin_versant", "annee", "code_nature_eau"),
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            user = "does")
commenter_table(comment = "calcul de la moyenne P90 par bassin versant",
                db = "datamart",
                schema = "nitrates",
                table = "nitrates_moyp90_bassin_versant_v0_13", 
                user = "does")

poster_data(data = nitrates_maxP90_bassin_versant,
            db = "datamart",
            schema = "nitrates", 
            table = "nitrates_maxp90_bassin_versant_v0_13",
            pk = c("code_bassin_versant", "annee", "code_nature_eau"),
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            user = "does")
commenter_table(comment = "calcul du maximum P90 par bassin versant",
                db = "datamart",
                schema = "nitrates",
                table = "nitrates_maxp90_bassin_versant_v0_13", 
                user = "does")


# creation de fichiers csv ------
date <- Sys.Date()
write.csv2(nitrates_P90, file = paste0("nitrates_P90_v0_13",date,".csv"))
write.csv2(nitrates_tendanceP90, file = paste0("nitrates_tendanceP90_v0_13",date,".csv"))
write.csv2(nitrates_moyP90_bassin_versant, file = paste0("nitrates_moyP90_bassin_versant_v0_13",date,".csv"))
write.csv2(nitrates_maxP90_bassin_versant, file = paste0("nitrates_maxP90_bassin_versant_v0_13",date,".csv"))


